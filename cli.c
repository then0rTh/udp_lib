/****************** CLIENT CODE ****************/
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
// #include "sock.h"
#include "flow.h"
#include "md5.h"

#define SEND_PORT 8888
#define LISTEN_PORT 8887
#define ADDR "127.0.0.1"
// #define ADDR "147.32.221.20"


int main(){
    listen_on(ADDR, LISTEN_PORT, SEND_PORT);

    char buffer[VALIDITY_SIZE + 1] = { 0 };
    unsigned char hash[32];
    get_md5("to_send", hash);
    for (int i = 0; i < 32; i++) printf("%c ", hash[i]);
    printf("\n");

    FILE *f = fopen("to_send", "rb");
    char meta[32 + 2*sizeof(int64_t)];
    memcpy(meta, hash, 32);
    int64_t *meta_ = (int64_t*) &meta[32];
    fseek(f, 0, SEEK_END); // seek to end of file
    meta_[0] = ftell(f); // get current file pointer
    fseek(f, 0, SEEK_SET); // seek back to beginning of file
    meta_[1] = 0;

    int got;
    got = my_flow_send_to(meta, sizeof(meta), ADDR);
    printf("send %d\n", got);
    do {
        got = fread(buffer, sizeof(char), VALIDITY_SIZE, f);
        if (got <= 0) break;
        buffer[got] = '\0';
        got = my_flow_send_to(buffer, got, ADDR);
        printf("Data sent(%i)\n", got);
        // printf("Data sent(%i): %s\n", got, buffer);
    } while(got > 0);

    fclose(f);

    get_md5("to_send", hash);
    for (int i = 0; i < 32; i++) printf("%c ", hash[i]);
    printf("\n");
    return 0;
}
