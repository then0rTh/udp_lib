#include "flow.h"

void send_confirm();
void send_failed();
bool got_confirm();


uint32_t current_packet = 0;

char FLOW_BUFFER[VALIDITY_SIZE] = { 0 };
char *FLOW_DATA = &FLOW_BUFFER[FLOW_ADDIN];
uint32_t *FLOW_ID = (uint32_t*)FLOW_BUFFER;
// int CURRENT_SIZE = 0;

int
my_flow_recv(char *buffer)
{
    int got;
    while (true){
        got = my_valid_recv(FLOW_BUFFER);
        if (got >= FLOW_ADDIN) break;
        send_failed();
    }
    send_confirm();
    memcpy(buffer, FLOW_DATA, got - FLOW_ADDIN);
    return got - FLOW_ADDIN;
}

int
my_flow_send_to(char *buffer, int size, const char *address)
{
    memcpy(FLOW_DATA, buffer, size);
    (*FLOW_ID)++;
    int got;
    while (true) {
        got = my_valid_send_to(FLOW_BUFFER, size+FLOW_ADDIN, address);
        if (got_confirm()) break;
    }
    return got - FLOW_ADDIN;
}


/////////////////////////////////////////////////////
char OK_conf[3] = {255, 255, 255};
char NOT_conf[3] = {0, 0, 0};

void
send_confirm()
{
    printf("sending confirm\n");
    my_reply(OK_conf, 3);
}

void
send_failed()
{
    printf("sending fail\n");
    my_reply(NOT_conf, 3);
}

bool
got_confirm()
{
    char conf[3] = { 0 };
    printf("w8ing for confirm\n");
    return my_recv_f(conf, 0) == 3 &&
        (
            (conf[0] == 255) +
            (conf[1] == 255) +
            (conf[2] == 255)
        ) >= 2;
}
