/****************** SERVER CODE ****************/
#include <stdio.h>
#include <string.h>
#include <unistd.h>
// #include "sock.h"
#include "flow.h"
#include "md5.h"

#define SEND_PORT 8887
#define LISTEN_PORT 8888
#define ADDR "127.0.0.1"
// #define ADDR "147.32.221.20"


int main(){
    listen_on(ADDR, LISTEN_PORT, SEND_PORT);

    char buffer[VALIDITY_SIZE+1] = { 0 };

    FILE *f = fopen("incoming", "wb");

    int got;
    got = my_flow_recv(buffer);
    printf("got %d\n", got);
    int64_t* meta = (int64_t*) &buffer[32];
    int64_t file_size = meta[0];
    int64_t connection_id = meta[1];
    unsigned char hash[32];
    memcpy(hash, buffer, 32);
    for (int i = 0; i < 32; i++) printf("%c ", hash[i]);
    printf("\n");
    while (file_size > 0) {
        got = my_flow_recv(buffer);
        // got = my_recv(buffer, BUFF_SIZE);
        file_size -= got;
        buffer[got] = '\0';
        printf("Got(%i)\n", got);
        // printf("Got(%i): %s\n", got, buffer);
        fwrite(buffer, sizeof(char), got, f);
    }// while (got > 0);

    if (fclose(f) != 0) printf("fclose failed\n");
    unsigned char actual_hash[32];
    get_md5("incoming", actual_hash);
    for (int i = 0; i < 32; i++) printf("%c ", actual_hash[i]);
    printf("\n");
    if (strncmp(hash, actual_hash, 32) != 1)
        printf("wrong hash\n");
    else
        printf("hash OK\n");

    return 0;
}
