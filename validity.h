#ifndef VALIDITY_HEADER
#define VALIDITY_HEADER

#include "sock.h"

#define VALIDITY_ADDIN 4
#define VALIDITY_SIZE (BUFF_SIZE - VALIDITY_ADDIN)



int
my_valid_recv(char *buffer);


int
my_valid_send_to(char *buffer, int size, const char *address);

/////////////////////////////////
bool
is_valid();

void
add_validity();

#endif
