#ifndef SOCK_HEADER
#define SOCK_HEADER

#include <sys/socket.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


typedef int t_sock;
#define BUFF_SIZE 512


// struct metadata {
//     int64_t file_size;
//     int64_t connection_id;
// };


int
my_recv(char *buffer);
int
my_recv_f(char *buffer, int flags);


int
my_send_to(char *buffer, int size, const char *address);
int
my_reply(char *buffer, int size);


int
listen_on(const char *address, int listen_port, int send_port);


//////////////////////////////////////
struct sockaddr_in
get_socket_address(const char *addr, int port);


t_sock
get_socket();


#endif
