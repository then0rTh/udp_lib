#include <arpa/inet.h>
#include <string.h>
#include "sock.h"

t_sock listening_socket = -1;

const char *listening_to;
int LISTEN_PORT = 0;
int SEND_PORT = 0;


int
my_recv_f(char *buffer, int flags)
{
    if (listening_socket == -1) return -1;
    return recvfrom(listening_socket, buffer, BUFF_SIZE, flags, NULL, 0);
}

int
my_recv(char *buffer)
{
    return my_recv_f(buffer, 0);
}


int
my_send_to(char *buffer, int size, const char *address)
{
    struct sockaddr_in addr = get_socket_address(address, SEND_PORT);
    return sendto(get_socket(), buffer, size, 0, (struct sockaddr*)&addr, sizeof addr);
}

int
my_reply(char *buffer, int size)
{
    return my_send_to(buffer, size, listening_to);
}


int
listen_on(const char *address, int listen_port, int send_port)
{
    LISTEN_PORT = listen_port;
    SEND_PORT = send_port;
    listening_to = address;
    struct sockaddr_in addr = get_socket_address(address, LISTEN_PORT);
    listening_socket = get_socket();
    return bind(listening_socket, (struct sockaddr *) &addr, sizeof addr);
}


////////////////////////////////////
struct sockaddr_in
get_socket_address(const char *addr, int port)
{
    struct sockaddr_in serverAddr;
    /*---- Configure settings of the server address struct ----*/
    /* Address family = Internet */
    serverAddr.sin_family = AF_INET;
    /* Set port number, using htons function to use proper byte order */
    serverAddr.sin_port = htons(port);
    /* Set IP address to localhost */
    serverAddr.sin_addr.s_addr = inet_addr(addr);
    /* Set all bits of the padding field to 0 */
    memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);

    return serverAddr;
}


t_sock
get_socket()
{
    return socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
}
