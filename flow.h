#ifndef FLOW_HEADER
#define FLOW_HEADER

#include "validity.h"

#define FLOW_ADDIN 4
#define FLOW_SIZE (VALIDITY_SIZE - FLOW_ADDIN)

int
my_flow_recv(char *buffer);

int
my_flow_send_to(char *buffer, int size, const char *address);


#endif
