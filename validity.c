#include "validity.h"

// void send_confirm();
// void send_failed();
// bool got_confirm(const char *address);


char VALID_BUFFER[BUFF_SIZE] = { 0 };
char *VALID_DATA = &VALID_BUFFER[VALIDITY_ADDIN];
int CURRENT_SIZE = 0;

// int _min(int a, int b) { return a < b ? a : b; }

// fill VALID_BUFFER from sock.h, remove metadata and copy data to buffer
// buffer must have VALIDITY_SIZE
int
my_valid_recv(char *buffer)
{
    int got;
    // while (true) {
        got = my_recv(VALID_BUFFER);  // fills VALID_BUFFER
        CURRENT_SIZE = got - VALIDITY_ADDIN;
        // if (CURRENT_SIZE <= 0 || !is_valid())
            // send_failed();
        // else
            // break;
    // }
    // send_confirm();
    if (!is_valid()) return -2;
    memcpy(buffer, VALID_DATA, CURRENT_SIZE);  //fill buffer with valid data
    return CURRENT_SIZE;
}


// fill VALID_DATA from buffer and prefix it with metadata, sent it whole thru sock.h
int
my_valid_send_to(char *buffer, int size, const char *address)
{
    if (size > VALIDITY_SIZE) exit(-2);
    memcpy(VALID_DATA, buffer, size);
    CURRENT_SIZE = size;
    add_validity();
    int sent;
    // do {
        sent = my_send_to(VALID_BUFFER, size + VALIDITY_ADDIN, address);
    // } while (!got_confirm(address));

    return sent - VALIDITY_ADDIN;
}


/////////////////////////////////
uint32_t
crc32b(char *message, int size)
{
    uint32_t byte, crc, mask;
    crc = 0xFFFFFFFF;
    for (int i = 0; i < size; i++) {
        byte = message[i];            // Get next byte.
        crc = crc ^ byte;
        for (int j = 7; j >= 0; j--) {    // Do eight times.
            mask = -(crc & 1);
            crc = (crc >> 1) ^ (0xEDB88320 & mask);
        }
        i = i + 1;
    }
    return ~crc;
}

bool
is_valid()
{
    return *(uint32_t*)VALID_BUFFER == crc32b(VALID_DATA, CURRENT_SIZE);
}

void
add_validity()
{
    uint32_t *crc = (uint32_t*)VALID_BUFFER;
    crc[0] = crc32b(VALID_DATA, CURRENT_SIZE);
}
//
// char OK_conf[3] = {255, 255, 255};
// char NOT_conf[3] = {0, 0, 0};
// void
// send_confirm()
// {
//     printf("sending confirm\n");
//     my_reply(OK_conf, 3);
// }
//
// void
// send_failed()
// {
//     printf("sending fail\n");
//     my_reply(NOT_conf, 3);
// }
//
// bool
// got_confirm(const char *address)
// {
//     char conf[3] = { 0 };
//     printf("w8ing for confirm\n");
//     return my_recv_f(conf, 0) == 3 &&
//         (
//             (conf[0] == 255) +
//             (conf[1] == 255) +
//             (conf[2] == 255)
//         ) >= 2;
// }
